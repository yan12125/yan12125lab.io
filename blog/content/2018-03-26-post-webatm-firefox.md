Title: 在Firefox 59上使用郵局的WebATM
Date: 2018-03-27 20:13
Authors: Chih-Hsuan Yen
Slug: post-webatm-firefox

---

1. 安裝郵局提供給Firefox/Chrome/Edge用的元件
2. 啟動元件
3. 匯入根憑證cacert.pem

進階玩法：VirtualBox NAT port forwarding

小小抱怨：WebATM元件一開機就會啟動，也不能取消，微不便
