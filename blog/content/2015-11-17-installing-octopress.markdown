Title: Installing Octopress
Date: 2015-11-17 00:54:31
Authors: Chih-Hsuan Yen
Slug: installing-octopress

弄個部落格放一些技術文章是我一直想做的事。之前試過幾個平台，例如PTT，Wordpress等等，都覺得差強人意。主要是貼code很不方便。有一次看到有人部落格用Octopress架站，好奇之下查了一些資料，發現他可以用Markdown寫文章，就下定決心要來試試看。

我測試的環境是家裡電腦，系統是Arch Linux。折騰了兩三個小時，終於把Octopress裝起來了。過程中最大的問題是Octopress 3.x之後用的指令都跟2.x完全不同，最後用的指令如下：

    $ git init
    $ octopress new .
    $ jekyll build
    $ octopress deploy init rsync
    $ vim _deploy.yml
    $ octopress deploy

其中關鍵的地方是第二行`octopress new .` 如果照著[官方教學](https://github.com/octopress/octopress/blob/master/README.md#init)用`octopress init .`，出來的HTML就沒有style。用以上步驟建出來的雖然不如[官網](http://octopress.org/)的那麼漂亮，但也不錯看了。

另外一個麻煩的地方是`_deploy.yml`的設定。由於標準的rsync URI要有兩個冒號，例如：

    :::text
    yen@localhost::octopress/

而Octopress的路徑是rsync over ssh，只有一個冒號，所以`_deploy.yml`的`remote_path`我另外加了一個冒號在前面。希望未來的版本能直接支援一般的rsync protocol。

架好之後，用`jekyll serve -w`就會在localhost:4000開一個server，markdown存檔時會自動重新build，相當方便。
