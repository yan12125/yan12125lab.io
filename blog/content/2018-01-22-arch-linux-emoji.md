Title: 解決Arch Linux下emoji的顯示
Date: 2018-01-22 14:46
Authors: Chih-Hsuan Yen
Slug: arch-linux-emoji

KeePassXC[在entry view裡加了不少欄位](https://github.com/keepassxreboot/keepassxc/pull/1305)，其中一個是[paperclip (📎, U+1F4CE)](https://emojipedia.org/paperclip/)。在macOS正常，但在Arch Linux無法正常顯示，只有一個方框。裝了[noto-fonts-emoji](https://www.archlinux.org/packages/extra/any/noto-fonts-emoji/)之後就正常了。

根據[Arch Linux Wiki](https://wiki.archlinux.org/index.php/Fonts_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)#X11.E4.B8.AD.E7.9A.84.E5.AD.97.E4.BD.93.E5.9B.9E.E6.BB.9A.E9.A1.BA.E5.BA.8F)的說明，fontconfig會自動選一個有emoji的字型，所以理論上系統只要裝一個就好。有看到一個比較花俏的[emoji字型](https://github.com/eosrei/twemoji-color-font)，改天有空再來玩玩。

裝了noto-fonts-emoji之後，xfwm4也可以正常顯示emoji了。現在剩下的問題就是[QTerminal/Konsole無法處理UCS-4](https://github.com/lxde/qtermwidget/pull/96)...
