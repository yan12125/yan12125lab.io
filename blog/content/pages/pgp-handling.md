Title: PGP Tips
Authors: Chih-Hsuan Yen
Slug: pgp-handling

## Things to do after updating my PGP key

* Send updated keys to major key servers (hkps://keyserver.ubuntu.com, hkps://keys.openpgp.org) and check whether keys are correcly uploaded or not
    - [Keys returned by keys.openpgp.net does not include signatures from others](https://keys.openpgp.org/about/faq#third-party-signatures)
    - Check keys uploaded to Ubuntu keyserver: [current key](https://keyserver.ubuntu.com/pks/lookup?search=0xE62545315B012B69C8C94A1D56EC201BFC794362&fingerprint=on&op=index) ([the raw key](http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xe62545315b012b69c8c94a1d56ec201bfc794362) is not updated immediately)
    - Check keys uploaded to keys.openpgp.org: [current key](https://keys.openpgp.org/vks/v1/by-fingerprint/E62545315B012B69C8C94A1D56EC201BFC794362)
    - keys.openpgp.org [does not distribute revocation certificates](https://keys.openpgp.org/about/faq#revoked-uids). Instead, user ID packets are gone after revocation, and thus `gpg --import` fails with `no user ID`.
* Submit updated public keys to [Arch Linux keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring/)
    - Create a merge request following the workflow for [modifying a packager key](https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/wikis/workflows/Modify-a-Packager-Key)
    - Check if updated keys are available via WKD using [wkd.mjs]({attach}/files/wkd.mjs). Note that WKD is updated only after a new archlinux-keyring version is tagged.

```shell
curl $(node wkd.mjs --email yan12125@archlinux.org --advanced) | gpg --list-packets
```

## Things to do after getting a new signature

* If there are signatures created by Arch Linux members, sync signatures from [Arch Linux keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring/)

```
$ cd archlinux-keyring
$ ./keyringctl export yan12125 | gpg --import
```

For signatures created by others, follow these steps:

* Ask the signer to send the signature to a keyserver
* Refresh my key from that keyserver

After importing new signature, publish the overall key via steps in the previous section.

## References

* [PGP Best Practices from Arch Linux](https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/wikis/best-practices)

By default, `hokey lint` uses colors to indicate warnings and errors. To see details, use `gpg --export "${FULL_PGP_FINGERPRINT}" | hokey lint --output-format=JSON | json_pp`.

* Somewhat out-dated [OpenPGP Best Practices](https://riseup.net/ru/security/message-security/openpgp/gpg-best-practices) from riseup.net

* [SKS Keyserver Network Under Attack](https://gist.github.com/rjhansen/67ab921ffb4084c865b3618d6955275f)
