Title: 搶救「統一發票兌獎」APP 資料
Date: 2024-08-22 23:45
Authors: Chih-Hsuan Yen
Slug: invoices-app-data-recovery

臺灣財政部出品的 [統一發票兌獎](https://play.google.com/store/apps/details?id=tw.gov.invoice) 近期更新後，需要重新登入，且手動掃描/輸入的發票，與超過一定期限的電子發票資料皆消失。Google Play 的部份相關評論下有以下回覆：

> 您好，因Google官方授權通訊協定(OAuth)政策異動，原兌獎APP之安卓裝置將無法進行備份，故新版本配合調整相關協定以符合Google政策要求，致安卓手機於更版後會有自動登出現象。有關會員卡及掃描/手動輸入發票，倘有自行「登出選保留」或有進行「資料備份」者皆能保有資料，若無進行上述操作則資料將無法還原，造成不便，尚祈見諒。

根據我的分析，原有數據都還在，只是新版 APP 不會去使用，因此一個可能的解法是使用 `adb install -d` 降級回舊版 APP，再將資料備份。然而，較久遠以前的電子發票仍無法存取。對此，若已取得 Android 系統的 root 權限，則可手動將舊版 APP 資料轉換為新版格式，以還原所有資料。

# Data migration steps

1. Close and force stop the app to avoid data corruption during migration

1. Get the old database

        ::sh
        cp -iv /data/data/tw.gov.invoice/files/InvoiceApp.db3 /sdcard/

1. Find out the old database key

        ::sh
        cat /data/data/tw.gov.invoice/files/uuidFile

1. Open the old database in [SQLite Browser](https://sqlitebrowser.org/). Here the old database is encrypted with the key found in the previous step using SQLCipher 3 defaults.

    ![Open the old encrypted database]({static}/images/open-encrypted-database.png)

1. Find out the new database key

        ::sh
        cat /data/data/tw.gov.invoice/files/uuidFile.uuid

1. Migrate the database to use the new key and SQLCipher 4 defaults. It can be done in SQLite Browser via Tools -> Set Encryption. Note that after this step SQLite Browser will reload the database, which now needs the new key and new defaults.

    ![Set encryption parameters to match the new database]({static}/images/set-encryption.png)

1. Make a backup of the database at the new path

        ::sh
        mv -iv /data/data/tw.gov.invoice/files/.config/InvoiceApp.db3{,.bak}

1. Copy the migrated database to the new path

        ::sh
        cp -iv /sdcard/InvoiceApp.db3 /data/data/tw.gov.invoice/files/.config/InvoiceApp.db3

1. Find out owner/group and SELlinux context for application data

        ::sh
        /system/bin/ls -alZ /data/data/tw.gov.invoice/files/.config/

1. Update owner/group and SELinux context of the copied file to match existing files. Note that here `u0_a261:u0_a261` and `u:object_r:app_data_file:s0:c5,c257,c512,c768` are just examples. Actual values should be checked from the previous step. More details can be found in [this article]({filename}2021-03-15-migrate-android-app-data.md).

        ::sh
        chown u0_a261:u0_a261 /data/data/tw.gov.invoice/files/.config/InvoiceApp.db3
        chmod 0600 /data/data/tw.gov.invoice/files/.config/InvoiceApp.db3
        /system/bin/chcon u:object_r:app_data_file:s0:c5,c257,c512,c768 /data/data/tw.gov.invoice/files/.config/InvoiceApp.db3

