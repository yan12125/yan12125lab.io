Title: 一窺內容農場SEO技巧
Date: 2021-06-03
Authors: Chih-Hsuan Yen
Slug: content-farm-connections
Category: Security

# 緣起

內容農場猖獗已久，近日尤甚。
加上各個內容農場善用 SEO 技巧，使得使用 Google 搜尋時，不時能見到內容農場的文章。
雖有 [終結內容農場](https://github.com/danny0838/content-farm-terminator) 等瀏覽器外掛，以及能搭配 [uBlacklist](https://github.com/iorate/ublacklist) 使用的 [Google-Chinese-Results-Blocklist](https://github.com/cobaltdisco/Google-Chinese-Results-Blocklist) 等清單，終究是治標不治本。
而近日在替 Google-Chinese-Results-Blocklist 編修清單時，發現了一個內容農場使用的 SEO 小技巧。
聊表分享，以資雜談。

# 探究

luoow, jishuwen, weiwenku, mdeditor, gushiciku 等站都是我手動維護的一份內容農場清單中的項目。
這幾個網域之間具有互相 HTTP 3xx 跳轉的關係。
藉由Wayback Machine，可以看出網域的變遷。

* jishuwen: 2020年九月底、十月初之間，由[自行收錄文章](https://web.archive.org/web/2020*/https://www.jishuwen.com/d/psBI)，改為[跳轉到 mdeditor](https://web.archive.org/web/2020*/https://www.jishuwen.com/d/pYBD/zh-tw)。
* luoow: 2021年五月上旬，由[自行收錄文章](https://web.archive.org/web/*/https://www.luoow.com/dc_hk/104791373)改為[跳轉到 gushiciku](https://web.archive.org/web/*/https://www.luoow.com/dc_tw/110023596)。
* mdeditor: 2021年五月上旬，由[自行收錄文章](https://web.archive.org/web/*/https://www.mdeditor.tw/pl/pcr2/zh-tw)，改為[跳轉到 gushiciku](https://web.archive.org/web/*/https://www.mdeditor.tw/pl/gvK3/zh-tw)。
* weiwenku: 先前是自行收錄文章，2021年四月底開始有[跳轉到 luoow](https://web.archive.org/web/*/https://www.weiwenku.org/d/107438579) 的紀錄。

而藉由 WHOIS 資料，也可以一窺各個網域之間的連繫。
其中一個網域註冊人為吴阳平，另一個網域的註冊人為 yangping wu。搜索這個名字會出現[此 Facebook 帳號](https://www.facebook.com/yangping.wu.148)，其中註明了此人與其中一個網域微文庫的關聯，似乎是同一個人。

<details>
<summary>
gushiciku WHOIS 資料
</summary>

```text
$ whois gushiciku.cn
Domain Name: gushiciku.cn
ROID: 20190517s10001s12057304-cn
Domain Status: ok
Registrant: 吴阳平
Registrant Contact Email: 185159865@qq.com
Sponsoring Registrar: 阿里云计算有限公司（万网）
Name Server: jill.ns.cloudflare.com
Name Server: dave.ns.cloudflare.com
Registration Time: 2019-05-17 10:45:36
Expiration Time: 2022-05-17 10:45:36
DNSSEC: unsigned
```

</details>

<details>
<summary>
mdeditor WHOIS 資料
</summary>

```text
$ whois mdeditor.tw
Domain Name: mdeditor.tw
   Domain Status: ok
   Registrant:
      yangping wu  896500373@qq.com

   Administrative Contact:
      yangping wu  896500373@qq.com

   Technical Contact:
      yangping wu  896500373@qq.com

   Record expires on 2023-05-14 17:09:45 (UTC+8)
   Record created on 2020-05-14 17:09:45 (UTC+8)

   Domain servers in listed order:
      dave.ns.cloudflare.com
      jill.ns.cloudflare.com

Registration Service Provider: GoDaddy
Registration Service URL: http://www.GoDaddy.com/

Provided by NeuStar Registry Gateway Services
```

</details>

<details>
<summary>
luoow WHOIS 資料
</summary>

```text
$ whois luoow.com
   Domain Name: LUOOW.COM
   Registry Domain ID: 2419015362_DOMAIN_COM-VRSN
   Registrar WHOIS Server: grs-whois.hichina.com
   Registrar URL: http://www.net.cn
   Updated Date: 2020-12-19T05:23:00Z
   Creation Date: 2019-08-01T10:34:54Z
   Registry Expiry Date: 2021-08-01T10:34:54Z
   Registrar: Alibaba Cloud Computing (Beijing) Co., Ltd.
   Registrar IANA ID: 420
   Registrar Abuse Contact Email: DomainAbuse@service.aliyun.com
   Registrar Abuse Contact Phone: +86.95187
   Domain Status: ok https://icann.org/epp#ok
   Name Server: DAVE.NS.CLOUDFLARE.COM
   Name Server: JILL.NS.CLOUDFLARE.COM
   DNSSEC: unsigned
   URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of whois database: 2021-06-03T16:00:00Z <<<

For more information on Whois status codes, please visit https://icann.org/epp

NOTICE: The expiration date displayed in this record is the date the
registrar's sponsorship of the domain name registration in the registry is
currently set to expire. This date does not necessarily reflect the expiration
date of the domain name registrant's agreement with the sponsoring
registrar.  Users may consult the sponsoring registrar's Whois database to
view the registrar's reported date of expiration for this registration.

TERMS OF USE: You are not authorized to access or query our Whois
database through the use of electronic processes that are high-volume and
automated except as reasonably necessary to register domain names or
modify existing registrations; the Data in VeriSign Global Registry
Services' ("VeriSign") Whois database is provided by VeriSign for
information purposes only, and to assist persons in obtaining information
about or related to a domain name registration record. VeriSign does not
guarantee its accuracy. By submitting a Whois query, you agree to abide
by the following terms of use: You agree that you may use this Data only
for lawful purposes and that under no circumstances will you use this Data
to: (1) allow, enable, or otherwise support the transmission of mass
unsolicited, commercial advertising or solicitations via e-mail, telephone,
or facsimile; or (2) enable high volume, automated, electronic processes
that apply to VeriSign (or its computer systems). The compilation,
repackaging, dissemination or other use of this Data is expressly
prohibited without the prior written consent of VeriSign. You agree not to
use electronic processes that are automated and high-volume to access or
query the Whois database except as reasonably necessary to register
domain names or modify existing registrations. VeriSign reserves the right
to restrict your access to the Whois database in its sole discretion to ensure
operational stability.  VeriSign may restrict or terminate your access to the
Whois database for failure to abide by these terms of use. VeriSign
reserves the right to modify these terms at any time.

The Registry database contains ONLY .COM, .NET, .EDU domains and
Registrars.
Domain Name: luoow.com
Registry Domain ID: 2419015362_DOMAIN_COM-VRSN
Registrar WHOIS Server: grs-whois.hichina.com
Registrar URL: http://whois.aliyun.com
Updated Date: 2020-07-08T12:42:47Z
Creation Date: 2019-08-01T10:34:54Z
Registrar Registration Expiration Date: 2021-08-01T10:34:54Z
Registrar: Alibaba Cloud Computing (Beijing) Co., Ltd.
Registrar IANA ID: 420
Reseller:
Domain Status: ok https://icann.org/epp#ok
Registrant City:
Registrant State/Province: Hu Nan
Registrant Country: CN
Registrant Email:https://whois.aliyun.com/whois/whoisForm
Registry Registrant ID: Not Available From Registry
Name Server: DNS11.HICHINA.COM
Name Server: DNS12.HICHINA.COM
DNSSEC: unsigned
Registrar Abuse Contact Email: DomainAbuse@service.aliyun.com
Registrar Abuse Contact Phone: +86.95187
URL of the ICANN WHOIS Data Problem Reporting System: http://wdprs.internic.net/
>>>Last update of WHOIS database: 2021-06-03T16:00:22Z <<<

For more information on Whois status codes, please visit https://icann.org/epp

Important Reminder: Per ICANN 2013RAA`s request, Hichina has modified domain names`whois format of dot com/net/cc/tv, you could refer to section 1.4 posted by ICANN on http://www.icann.org/en/resources/registrars/raa/approved-with-specs-27jun13-en.htm#whois The data in this whois database is provided to you for information purposes only, that is, to assist you in obtaining information about or related to a domain name registration record. We make this information available "as is," and do not guarantee its accuracy. By submitting a whois query, you agree that you will use this data only for lawful purposes and that, under no circumstances will you use this data to: (1)enable high volume, automated, electronic processes that stress or load this whois database system providing you this information; or (2) allow, enable, or otherwise support the transmission of mass unsolicited, commercial advertising or solicitations via direct mail, electronic mail, or by telephone.  The compilation, repackaging, dissemination or other use of this data is expressly prohibited without prior written consent from us. We reserve the right to modify these terms at any time. By submitting this query, you agree to abide by these terms.For complete domain details go to:http://whois.aliyun.com/whois/domain/hichina.com
```

</details>

<details>
<summary>
gushiciku WHOIS 資料
</summary>

```text
$ whois jishuwen.com
   Domain Name: JISHUWEN.COM
   Registry Domain ID: 2313252870_DOMAIN_COM-VRSN
   Registrar WHOIS Server: grs-whois.hichina.com
   Registrar URL: http://www.net.cn
   Updated Date: 2020-09-17T01:35:51Z
   Creation Date: 2018-09-23T14:25:57Z
   Registry Expiry Date: 2021-09-23T14:25:57Z
   Registrar: Alibaba Cloud Computing (Beijing) Co., Ltd.
   Registrar IANA ID: 420
   Registrar Abuse Contact Email: DomainAbuse@service.aliyun.com
   Registrar Abuse Contact Phone: +86.95187
   Domain Status: ok https://icann.org/epp#ok
   Name Server: DAVE.NS.CLOUDFLARE.COM
   Name Server: JILL.NS.CLOUDFLARE.COM
   DNSSEC: unsigned
   URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of whois database: 2021-06-03T16:02:35Z <<<

For more information on Whois status codes, please visit https://icann.org/epp

NOTICE: The expiration date displayed in this record is the date the
registrar's sponsorship of the domain name registration in the registry is
currently set to expire. This date does not necessarily reflect the expiration
date of the domain name registrant's agreement with the sponsoring
registrar.  Users may consult the sponsoring registrar's Whois database to
view the registrar's reported date of expiration for this registration.

TERMS OF USE: You are not authorized to access or query our Whois
database through the use of electronic processes that are high-volume and
automated except as reasonably necessary to register domain names or
modify existing registrations; the Data in VeriSign Global Registry
Services' ("VeriSign") Whois database is provided by VeriSign for
information purposes only, and to assist persons in obtaining information
about or related to a domain name registration record. VeriSign does not
guarantee its accuracy. By submitting a Whois query, you agree to abide
by the following terms of use: You agree that you may use this Data only
for lawful purposes and that under no circumstances will you use this Data
to: (1) allow, enable, or otherwise support the transmission of mass
unsolicited, commercial advertising or solicitations via e-mail, telephone,
or facsimile; or (2) enable high volume, automated, electronic processes
that apply to VeriSign (or its computer systems). The compilation,
repackaging, dissemination or other use of this Data is expressly
prohibited without the prior written consent of VeriSign. You agree not to
use electronic processes that are automated and high-volume to access or
query the Whois database except as reasonably necessary to register
domain names or modify existing registrations. VeriSign reserves the right
to restrict your access to the Whois database in its sole discretion to ensure
operational stability.  VeriSign may restrict or terminate your access to the
Whois database for failure to abide by these terms of use. VeriSign
reserves the right to modify these terms at any time.

The Registry database contains ONLY .COM, .NET, .EDU domains and
Registrars.
Domain Name: jishuwen.com
Registry Domain ID: 2313252870_DOMAIN_COM-VRSN
Registrar WHOIS Server: grs-whois.hichina.com
Registrar URL: http://whois.aliyun.com
Updated Date: 2019-09-17T05:56:35Z
Creation Date: 2018-09-23T14:25:57Z
Registrar Registration Expiration Date: 2021-09-23T14:25:57Z
Registrar: Alibaba Cloud Computing (Beijing) Co., Ltd.
Registrar IANA ID: 420
Reseller:
Domain Status: ok https://icann.org/epp#ok
Registrant City:
Registrant State/Province: Hu Nan
Registrant Country: CN
Registrant Email:https://whois.aliyun.com/whois/whoisForm
Registry Registrant ID: Not Available From Registry
Name Server: DAVE.NS.CLOUDFLARE.COM
Name Server: JILL.NS.CLOUDFLARE.COM
DNSSEC: unsigned
Registrar Abuse Contact Email: DomainAbuse@service.aliyun.com
Registrar Abuse Contact Phone: +86.95187
URL of the ICANN WHOIS Data Problem Reporting System: http://wdprs.internic.net/
>>>Last update of WHOIS database: 2021-06-03T16:02:53Z <<<

For more information on Whois status codes, please visit https://icann.org/epp

Important Reminder: Per ICANN 2013RAA`s request, Hichina has modified domain names`whois format of dot com/net/cc/tv, you could refer to section 1.4 posted by ICANN on http://www.icann.org/en/resources/registrars/raa/approved-with-specs-27jun13-en.htm#whois The data in this whois database is provided to you for information purposes only, that is, to assist you in obtaining information about or related to a domain name registration record. We make this information available "as is," and do not guarantee its accuracy. By submitting a whois query, you agree that you will use this data only for lawful purposes and that, under no circumstances will you use this data to: (1)enable high volume, automated, electronic processes that stress or load this whois database system providing you this information; or (2) allow, enable, or otherwise support the transmission of mass unsolicited, commercial advertising or solicitations via direct mail, electronic mail, or by telephone.  The compilation, repackaging, dissemination or other use of this data is expressly prohibited without prior written consent from us. We reserve the right to modify these terms at any time. By submitting this query, you agree to abide by these terms.For complete domain details go to:http://whois.aliyun.com/whois/domain/hichina.com
```

</details>

我猜搜尋引擎對網站的排名會隨著 HTTP 3xx 跳轉而轉移。藉由 3xx 跳轉不斷變換網域，能在保留網域排名的情況下不斷規避封鎖清單。或許未來封鎖清單能自動追蹤網域跳轉，在新網域獲得足夠的排名前先行封鎖。

無論如何，瀏覽器外掛終究需要使用人數多才能發揮效益。這應是過於樂觀了──當這種情況成真時，或許多數人已經了解抵制內容農場的重要性，內容農場的消亡也不遠了。
