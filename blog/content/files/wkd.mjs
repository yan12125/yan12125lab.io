/*
 * Compute WKD URLs
 * https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/
 * Section 3.1
 */
import { parseArgs } from 'node:util';
import zbase32 from 'zbase32';
import crypto from 'crypto';

const options = {
  direct: { type: 'boolean' },
  advanced: { type: 'boolean' },
  email: { type: 'string' },
};
const { values } = parseArgs({ options });

let [local_part, domain] = values.email.split('@');

let shasum = crypto.createHash('sha1');
shasum.update(local_part.toLowerCase());
let result = shasum.digest();

if (values.advanced) {
    // advanced method
    console.log(`https://openpgpkey.${domain}/.well-known/openpgpkey/${domain.toLowerCase()}/hu/${zbase32.encode(result)}?l=${local_part}`);
} else if (values.direct) {
    // direct method (the sub-domain "openpgpkey" does not exist)
    console.log(`https://${domain}/.well-known/openpgpkey/hu/${zbase32.encode(result)}?l=${local_part}`);
}
