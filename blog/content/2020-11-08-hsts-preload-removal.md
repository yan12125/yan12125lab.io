Title: Removal from HSTS preload list for my domain
Date: 2020-11-08 14:24
Authors: Chih-Hsuan Yen
Category: Security
Slug: hsts-preload-removal

# Motivation

Since long time ago, I host my website on my own server.
As time goes by, the maintaining cost for this website appears to increase indefinitely with complexity from Podman, Ansible, Buildbot and so on.
I have once tried use GitLab pages to host my website.
However, it does not support [adding custom HSTS headers](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/28), so I need to opt-out HSTS preloading if I don't want to see a warning on [hstspreload.org](https://hstspreload.org).

# Timeline

Opting-out HSTS preloading is as simple as opting-in.
I just changed the `Strict-Transport-Security` header for my server and submit a request at hstspreload.org.
As it is said that the removal can take a long time, I'm curious how long it will.
So, here is the timeline.

* 2020/11/08 submitted removal request
* 2020/11/14 [Removed from Chromium source](https://chromium.googlesource.com/chromium/src/net/+/acd756e37a432016a1078a73edecb12ac26f13a6^!/)
* 2020/11/17 [Removed from Firefox source](https://hg.mozilla.org/mozilla-central/rev/61fadacfb1aff53f8f79cfabe6a6fe5eb03e06fa)
