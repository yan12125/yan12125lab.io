Title: [轉載] Containers 系統架構
Date: 2021-03-23
Authors: Chih-Hsuan Yen
Slug: containers-architecture

Rootless containers 是一項最近還滿常被討論的技術。
比較常見的實作當推 RedHat 主打的 [Podman](https://podman.io/)。
在學習 Podman 的過程中，被 container 架構的複雜性搞的七葷八素。
查遍各處，寫的比較完善的中文解說莫屬第 11 屆 iT 邦幫忙鐵人賽參賽作品：[其實我真的沒想過在美國上班也被拉來參加30天分享那些年我怎麼理解 kubernetes 的運作](https://ithelp.ithome.com.tw/users/20120317/ironman/2442)。
這一系列的文章分門別類講述 container stack 中的不同元件，再輔以歷史的脈絡探討架構演變的過程中，主事者可能的心路歷程。
詳盡而不流於雜亂，且能看出作者的技術功力深厚，個人認為是上乘之作！
