Title: What's inside a backup file of IPMIView on Android?
Date: 2021-03-07
Authors: Chih-Hsuan Yen
Slug: ipmiview-android
Category: Work

[IPMIView](https://play.google.com/store/apps/details?id=com.smc.smcipmitool) is a handy tool to monitor status of multiple servers at once.
It provides a backup feature to export settings and credentials.
Per `file` and `bsdtar` commands, the backup file `/sdcard/Supermicro/IPMIViewBackup_YYYYMMDDHHMMSS.smc` is a password-protected zip archive.
Assuming the password can be extracted from the APK, I started a journey of reverse engineering.

First, retrieve the APK file for IPMIView:

    ::shell-session
    $ adb shell pm list packages | grep -i ipmi
    package:com.smc.smcipmitool
    $ adb shell pm path com.smc.smcipmitool
    package:/data/app/com.smc.smcipmitool-MffdfTl97aivo2O1HplX2g==/base.apk
    $ adb pull "/data/app/com.smc.smcipmitool-MffdfTl97aivo2O1HplX2g==/base.apk"
    $ mv base.apk com.smc.smcipmitool.apk

And let [apktool](https://ibotpeaches.github.io/Apktool/) extract smali codes:

    ::shell-session
    $ apktool d com.smc.smcipmitool.apk

Next, find out which classes are related to the backup file:

    ::shell-session
    $ rg -l IPMIViewBackup com.smc.smcipmitool
    com.smc.smcipmitool/smali/com/smc/smcipmitool/util/IPMIBackUpAgent$3.smali
    com.smc.smcipmitool/smali/com/smc/smcipmitool/util/IPMIBackUpAgent.smali
    com.smc.smcipmitool/smali/com/smc/smcipmitool/util/IPMIBackUpAgent$1.smali

Finally, searching for 'password' in these files. Bingo!
In `com.smc.smcipmitool/smali/com/smc/smcipmitool/util/IPMIBackUpAgent$1.smali`, there is a function call:

    ::smali
    move-result-object v1

    const-string v2, "8o2r2c6i5M9r9e0puS"

    invoke-virtual {v1, v5, v3, v2}, Lcom/smc/smcipmitool/util/IPMIBackUpAgentHelper;->zipDirWithPassword([Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Z

So, the password is here. I can use that password to extract the compressed IPMIViewBackup.smc

    ::shell-session
    $ bsdtar -xf IPMIViewBackup_YYYYMMDDHHMMSS.smc IPMIViewBackup.smc

It's yet another zip archive.

    ::shell-session
    $ bsdtar tf IPMIViewBackup.smc
    group.xml
    shared_prefs/
    shared_prefs/session_pref.xml
    shared_prefs/com.smc.smcipmitool_preferences.xml

IPMI server IPs, encoded usernames and passwords can be found in the file `group.xml`.


_Elementary, My Dear Watson. - [not by Sherlock Holmes](https://quoteinvestigator.com/2016/07/14/watson/)_
