Title: 網站搬家
Date: 2018-06-01 11:59
Authors: Chih-Hsuan Yen
Slug: domain-migraion

今天收到Let's Encrypt的通知信，說chyen.cc和\*.chyen.cc的SSL憑證20天後過期。我這個網域名稱是在Namesilo買的。由於Let's Encrypt的wildcard certificate需要用DNS認證，然而certbot[還不支援Namesilo](https://github.com/certbot/certbot/issues/4595)，所以我上次是手動把TXT紀錄一個一個(註：每個domain name會對應一個DNS challenge)填上去。這次決定把DNS改由CloudFlare代管，用certbot的dns-cloudflare外掛直接取得新的SSL憑證。

會選CloudFlare一個原因是我有考慮未來我的[Arch Linux個人repo](https://dl.chyen.cc/arch-repo/)可能會需要開CDN，先把DNS搬過去之後就省的再搬。

CloudFlare的教學我是參考[香腸大的文章](https://sofree.cc/cloudflare-free-cdn/)，基本上設定滿簡單易懂的，新增DNS record後，propagate的時間也很短，大約幾秒到幾十秒而已，比Namesilo弄每個record都讓我等了好幾分鐘好多了。一個小小的缺點是，CloudFlare設定兩階段驗證只給QR code，不提供直接給TOTP secret的選項。為了把TOTP存進KeePassXC資料庫中，只好用[zbar](https://github.com/procxx/zbar)把QR code還原回TOTP URI。

接著用certbot請求一個新的SSL憑證。第一次遇到" The currently selected ACME CA endpoint does not support issuing wildcard certificates."的錯誤。原來是因為技術問題，certbot[到現在還沒預設為ACMEv2](https://github.com/certbot/certbot/issues/5369)。這讓我想起了當初ACMEv2[因為安全性問題延後發布](https://community.letsencrypt.org/t/acmev2-and-wildcard-launch-delay/53654)。ACMEv2還真是命運多舛啊！

發好新憑證之後，順便把一些原本跑在chyen.csie.org的服務改到chyen.cc上。

第一個是Quassel IRC server。把[新憑證複製過去](https://github.com/certbot/certbot/issues/1250)，Quassel重新啟動就完成了，沒遇到什麼問題。

接下來是Postfix mail server。我把main.cf改好，去Nextcloud, BuildBot, TT-RSS等地方把寄件者的domain name改成新的，以為這樣就完工了，實際測試寄信的時候卻跳出cannot connect的錯誤。這時我才想起我防火牆沒開Postfix，因此需要到/etc/hosts把"127.0.0.1 chyen.cc"加進去。

這時候，寄信到gmail沒問題，寄到hotmail卻被當成垃圾信。查了一下發現DNS反解忘了改。到Linode把DNS反解也改成新的網址之後，hotmail還是不認我的信。目前可能只能先用白名單擋著了。

最後是這個部落格。改一下pelican跟Apache的設定檔就好。也順便[加上了RSS feed](https://github.com/yan12125/blog/issues/1)並把預設template裡面沒用的指令刪掉。

搞這些花了我一個晚上。運維真的是件頗麻煩的工作XD
