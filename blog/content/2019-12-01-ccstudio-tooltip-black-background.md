Title: A workaround for CCStudio 9.2 UI issue - black backgrounds in tooltips
Date: 2019-12-01 12:54
Authors: Chih-Hsuan Yen
Slug: ccstudio-tooltip-black-background

[CCStudio](https://www.ti.com/tool/CCSTUDIO) is what our lab use for developing software on [TI MSP430](https://www.ti.com/microcontrollers/msp430-ultra-low-power-mcus/overview.html) board. Recently, version 9.2.0 of CCStudio is out, and I noticed that its UI now uses GTK+3 instead of GTK+2. In general this is a good change as GTK+3 has a better look-and-feel by default than GTK+2. However, soon I noticed an issue. Tooltips, which appear when you hover around functions or variables in the code editor, have black backgrounds. As texts in tooltips are also black or in similar colors, tooltips are not readable.

I wonder if this is an incompatibility with CCStudio and GTK+3. First I tried uninstalling [gtk3](https://www.archlinux.org/packages/extra/x86_64/gtk3/) from my system. Yes the UI goes back to GTK+2 and tooltips are readable again. Obviously it is not a long-term fix as several other software like Firefox need it. I took some time to study how CCStudio selects between GTK+2 and GTK+3. First I looked at files loaded by the ccstudio process via lsof. There are some \*gtk\*.so files, and libswt-pi-gtk-4880.so, which is from [Eclipse SWT](https://www.eclipse.org/swt/), seems the most relevant. The source code of SWT can be found [here](https://git.eclipse.org/c/platform/eclipse.platform.swt.git/). As the SWT version used in CCStudio is `3.107.0.v20180611-0422`, I picked git tag `I20180611-0500` and searched for [the configuration key for the GTK+ version](https://wiki.eclipse.org/SWT/Devel/Gtk/GtkVersion) in this repository. After some code reading, I found that an environment variable `SWT_GTK3` [controls whether SWT prefers GTK+3 or GTK+2](https://git.eclipse.org/c/platform/eclipse.platform.swt.git/tree/bundles/org.eclipse.swt/Eclipse%20SWT%20PI/gtk/org/eclipse/swt/internal/gtk/OS.java?h=I20180611-0500#n76).

So, a long-term fix is simple - starting CCStudio with `SWT_GTK3=0`.

Update 2020/01/08 - the issue appears to be fixed in CCStudio 9.3.
