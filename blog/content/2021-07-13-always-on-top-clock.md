Title: Linux 與 Windows 下顯示在螢幕最上層的時鐘
Date: 2021-07-13
Authors: Chih-Hsuan Yen
Slug: always-on-top-clock
Category: Work

最近跟老師開會，常常討論太久，超過老師應該要接小孩的時間。要怎麼避免類似的情況再發生呢？我覺得可以在用投影片報告的時候，螢幕上放一個顯眼的時鐘。

在 Linux 上，我平常使用的 [devilspie2](https://www.nongnu.org/devilspie2/) 能夠讓特定的視窗維持最上層顯示。例如，使用 xclock 時，可以在 `$XDG_CONFIG_HOME/devilspie2/devilspie2.lua` 加入以下內容：

```lua
if window_class_lower == 'xclock' then
    make_always_on_top()
end
```

這邊 window class 可以用 `xprop` 指令確認。設定好之後，xclock 就能顯示在 LibreOffice 上了。

而在 Windows 上，我找了幾個時鐘程式，都沒辦法顯示在 PowerPoint 投影片放映時顯示在最上層。後來有人提到對時工具 NTPClock 有這個功能。一試之下，終於成功！

![NTPClock 中的「桌面最上層」選項]({static}/images/ntpclock-always-on-top.png)

#### 2022-12-23 update

`make_always_on_top()` uses `_NET_WM_STATE_ABOVE` [1](https://github.com/dsalt/devilspie2/blob/v0.44/src/script_functions.c#L415)[2](https://gitlab.gnome.org/GNOME/libwnck/-/blob/43.0/libwnck/window.c?ref_type=tags#L1668), so it needs [xfwm4 >= 4.18](https://gitlab.xfce.org/xfce/xfwm4/-/issues/686) or another standard-violating window manager to make xclock appear above libreoffice slideshow.
