Title: Tonalities
Date: 2014-09-17 18:56
Author: Chih-Hsuan Yen
Category: Uncategorized
Slug: tonalities
Status: published

Use my ear with the help of [vmpk](http://vmpk.sourceforge.net/), it's
not difficult to determine the tonality of popular songs. Here are
results:
<http://chyen.twbbs.org/yen/pub/major.html>

There is a Matlab toolbox developed by University of Jyväskylä named
[MIRtoolbox](https://www.jyu.fi/hum/laitokset/musiikki/en/research/coe/materials/mirtoolbox),
aiming to analyzing music clips with cutting-edge algorithms. It
provides a scientific way to determine the tonality. However, I still
like the primitive way :)
