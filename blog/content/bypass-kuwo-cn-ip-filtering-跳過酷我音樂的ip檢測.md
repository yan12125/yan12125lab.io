Title: Bypass kuwo.cn IP filtering 跳過酷我音樂的IP檢測
Date: 2015-02-25 21:41
Author: Chih-Hsuan Yen
Category: Security
Slug: bypass-kuwo-cn-ip-filtering-跳過酷我音樂的ip檢測
Status: published

[kuwo.cn](http://www.kuwo.cn) is a popular music sharing platform in
China. Chinese users can enjoy latest and high-quality music on it.
However, it exploits some trick to prevent users outside Mainland China
from listening music flawlessly. For example, when you visit
<http://www.kuwo.cn/yinyue/112882/> from a country different from China
(Taiwan in my case), you will be redirected to the homepage immediately
before you can click the play button and enjoy the relaxing voice. Out
of curiousity and my own need, I study a small portion of it source code
and find it simple to accomplish such IP filtering, and I also find out
a lightweight approach to bypass such restriction.

[酷我音樂](http://www.kuwo.cn)是一個中國著名的音樂分享平台。中國用戶可以在上面享受最新的高音質音樂。然而，這個網站使用了一些手段來防止中國大陸以外的用戶順利的聽音樂。舉例來說，如果你從一個中國以外的國家連上<http://www.kuwo.cn/yinyue/112882/>（就我的例子是台灣），你就會在按下播放鈕之前被重新導向到首頁，而無法享受這令人放鬆的嗓音。出於好奇心和我自己的需求，我對他的一小部份原始碼進行研究，發現他是用很簡單的方法來達成這種IP檢測。而我也找出一種輕巧的手段來跳過這種限制。

First, observing the source code of kuwo pages. By default it's not
possible to view the source code in the browser due to redirections, so
I download the page with curl:

首先，觀察這個網頁的原始碼。由於重新導向的緣故，預設情況下是無法用瀏覽器看到網頁原始碼，於是我用curl下載這個網頁：

    $ curl http://www.kuwo.cn/yinyue/112882/
    //...previous irrelevant codes omitted 省略前面無關的code...
        /*验证ip是否是国外四国的*/
        function checkIp(){
             jQuery.ajax({
                 url : '/yy/PlayCheckIp',
                 type : 'get',
                 data : "",
                 success : function(data){
                     if(data == "{\"stauts\":400}"){
                        window.location='http://www.kuwo.cn';
                     }
                 }
             });
        }   
        checkIp();
    //...some other codes 一些其他code...

Obviously, the site checks your IP and redirect you to their homepage if
you're not welcomed. You can visit <http://www.kuwo.cn/yy/PlayCheckIp>
and see what's in it. The simplest way to escape the awful situation is
blocking all traffic to the check point. If the checking request fails,
nothing happens. I have done it by adding a custom rule to
[AdblockPlus](https://adblockplus.org/). Now I can dive in the ocean of
music without annoying redirections!

顯然，這個網站會檢查你的IP，而且在你不受歡迎時把你重新導向到首頁。你可以連上<http://www.kuwo.cn/yy/PlayCheckIp>看看裡面有什麼。跳脫這種糟糕的情況最簡單的方法是阻擋所有通往檢查點的流量。如果檢查的請求沒有成功，什麼事都不會發生。我在[AdblockPlus](https://adblockplus.org/)裡加了一條自訂的過濾條件，然後就成功了。現在我可以徜徉音樂之海而不會遇到惱人的重新導向。

Other sites have some other tricks to prevent non-China users from
watching their videos or listening to their songs. Such examples include
[letv.com](http://www.letv.com/) and [youku.com](http://www.youku.com/).
Here are some examples:

其他網站有一些其他技巧來阻止非中國的使用者看他們的影片或聽他們的音樂。這樣的例子有[樂視](http://www.letv.com/)和[優酷](http://www.youku.com/)。這兒有些例子：

<http://www.letv.com/ptv/vplay/21511001.html>

There's a project [Unblock
Youku](https://github.com/zhuzhuor/Unblock-Youku) aiming on such issues.
However, kuwo.cn is not supported as of now. In fact, there's not need
to proxy the traffic of kuwo IP detection. Just block and ignore it.

有個專案[Unblock
Youku](https://github.com/zhuzhuor/Unblock-Youku)力圖解決這些問題，然而到目前為止酷我還沒被支援。實際上也沒有必要轉送酷我IP檢測的流量。只要阻擋然後忽略就行了。

Among these sites, what kuwo.cn uses is the simplest, but maybe it's
why there's no discussion about it. I googled PlayCheckIp and got
nothing meaningful. I hope this short post can help people outside
Mainland China have the same rights to enjoy great music :)

在這些網站裡，酷我用的方法是最簡單的，但這也可能是為何都沒有相關的討論。我在google上搜尋PlayCheckIp沒有得到有用的資訊。我希望這篇文章可以幫助中國大陸以外的使用者也能擁有享受美妙音樂的權利
:)
